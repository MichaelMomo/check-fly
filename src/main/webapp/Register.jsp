<%--
  Created by IntelliJ IDEA.
User: momo gardel
Date: 11/04/2020
Time: 21:40
To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>Create a new account</title>
</head>
<body style="background-image: url('img/background.jpg');">
<nav class="navbar navbar-expand navbar-light bg-light flex-column flex-md-row pipe-navbar justify-md-content-between">
    <a class="navbar-brand" href="index.jsp"><img src="https://pipe.bot/img/logo.svg" width="30" height="30" class="d-inline-block align-top" />  CHECK & FLY</a>
    <div class="navbar-nav-scroll ml-md-auto">
        <ul class="navbar-nav pipe-navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="" rel="noopener">About us</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="Register.jsp" rel="noopener">Registration</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="Login.jsp">Login</a>
            </li>
        </ul>
    </div>
</nav>
<div class="col d-flex justify-content-center">
    <div class="card" style="width: 38rem;">

        <h5 class="card-header info-color white-text text-center py-4" style="background-color: #4fa3e3;">
            <strong>Register</strong>
        </h5>
        <div class="card-body ">
            <form class="text-center" style="color: #757575;" action="RegistrationServlet"  method="post">

                <!-- Email -->
                <div class="md-form">
                    <label for="uname">E-mail</label>
                    <input type="email" id="uname" name="uname" class="form-control">

                </div>
                <!-- Password -->
                <div class="md-form">
                    <label for="psw">Password:</label>
                    <input type="password" id="psw" name="psw" class="form-control">
                </div>

                <div class="md-form">
                    <label for="psw_repeat">Repeat password:</label>
                    <input type="password" id="psw_repeat" name="psw_repeat" class="form-control">
                </div>


                <!-- Sign in button -->
                <button class="btn btn-outline-info btn-rounded btn-block my-4 waves-effect z-depth-0" type="submit">Register</button>


            </form>
        </div>
    </div>
</div>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>

