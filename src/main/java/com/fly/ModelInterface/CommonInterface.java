package com.fly.ModelInterface;


import java.io.IOException;
import java.util.List;

public interface CommonInterface<T, S> {
    public T get(List<T> t, S s);
    public S delete(List<T> t,S s);
    public String add(T t) throws IOException;
    public void getAll(List<T> t);
}
