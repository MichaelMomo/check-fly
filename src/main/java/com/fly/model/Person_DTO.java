package com.fly.model;

/**
 * @Person_DTO
 */

public abstract class Person_DTO {
    private String _name;
    private String _surname;
    private String _gender;
    private String _address;
    private String _dateOfBirth;
    private String _socialSNumber;

    public Person_DTO() {
        super();
    }

    public String get_name() {
        return _name;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public String get_surname() {
        return _surname;
    }

    public void set_surname(String _surname) {
        this._surname = _surname;
    }

    public String get_gender() {
        return _gender;
    }

    public void set_gender(String _gender) {
        this._gender = _gender;
    }

    public String get_address() {
        return _address;
    }

    public void set_address(String _address) {
        this._address = _address;
    }

    public String get_dateOfBirth() {
        return _dateOfBirth;
    }

    public void set_dateOfBirth(String _dateOfBirth) {
        this._dateOfBirth = _dateOfBirth;
    }

    public String get_socialSNumber() {
        return _socialSNumber;
    }

    public void set_socialSNumber(String _socialSNumber) {
        this._socialSNumber = _socialSNumber;
    }

    @Override
    public String toString() {
        return "Person_DTO{" +
                "_name='" + _name + '\'' +
                ", _surname='" + _surname + '\'' +
                ", _gender='" + _gender + '\'' +
                ", _address='" + _address + '\'' +
                ", _dateOfBirth='" + _dateOfBirth + '\'' +
                ", _socialSNumber='" + _socialSNumber + '\'' +
                '}';
    }
}