package com.fly.DBConnexion;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @EagerConnexion_singleton
 * this class implement a singleton like Eager for a connection on database postgreSQL
 *
 *  */

public class EagerConnexion_Singleton {


    private static final EagerConnexion_Singleton instance = new EagerConnexion_Singleton();
    private final static Logger log = Logger.getLogger(EagerConnexion_Singleton.class.getSimpleName());
    private Connection connection;
    private String url = "jdbc:postgresql://localhost:5432/dbtest";
    private String user = "test";
    private String password = "test";

    public EagerConnexion_Singleton() {
        try {
            try {
                Class.forName("org.postgresql.Driver");
                this.connection = DriverManager.getConnection(url, user, password);
            } catch (Exception e) {
                System.out.println("Database Connection Creation Failed : " + e.getMessage());
            }
        } catch (Exception e) {
            log.error(EagerConnexion_Singleton.class.getSimpleName() + ":" + "Database Connection Creation Failed : " + e.getMessage());
        }
    }

    public Connection getCon() {
        log.info(EagerConnexion_Singleton.class.getSimpleName() + ":" + "Database Connection Created with success.");
        return connection;
    }


    public static EagerConnexion_Singleton getInstance() {
        log.info(EagerConnexion_Singleton.class.getSimpleName() + ":" + "Connection Instance created");
        return instance;
    }
}
