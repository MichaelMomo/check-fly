package com.fly.DBConnexion;


import org.apache.log4j.Logger;


import java.sql.Connection;
import java.sql.DriverManager;


/**
 * @LasyConnexion_Singleton
 * this class implement a singleton like Eager for a connection on database postgreSQL
 */

public class LasyConnexion_Singleton  {

    private Connection connection;
    private final static Logger log = Logger.getLogger(LasyConnexion_Singleton.class.getSimpleName());
    private String url = "jdbc:postgresql://localhost:5432/dbtest";
    private String user = "test";
    private String password = "test";

            private static LasyConnexion_Singleton SINGLE_INSTANCE = null;

            private LasyConnexion_Singleton() {
                try {
                        Class.forName("org.postgresql.Driver");
                        this.connection = DriverManager.getConnection(url, user, password);
                    } catch (Exception e)
                    {
                        log.error(LasyConnexion_Singleton.class.getSimpleName() + ":" + "Database Connection Creation Failed : " + e.getMessage());
                    }
            }

           public Connection getConnection() {
                log.info(LasyConnexion_Singleton.class.getSimpleName() + ":" + "Database Connection Created with success.");
                return this.connection;
            }

          public static LasyConnexion_Singleton getInstance() {
               if (SINGLE_INSTANCE == null)
               {
                   synchronized (LasyConnexion_Singleton.class)
                   {
                        SINGLE_INSTANCE = new LasyConnexion_Singleton();
                   }
               }
               log.info(LasyConnexion_Singleton.class.getSimpleName() + ":" + "Connection Instance created");
               return SINGLE_INSTANCE;
            }
}
