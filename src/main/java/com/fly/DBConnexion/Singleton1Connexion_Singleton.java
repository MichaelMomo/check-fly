package com.fly.DBConnexion;
import org.apache.log4j.Logger;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


/**
 * @Singleton1Connexion_Singleton
 * this class implement a simple singleton  for a connection on database postgreSQL
 */

public class Singleton1Connexion_Singleton {
    private static Singleton1Connexion_Singleton singletonInstance ;
    private final static Logger log = Logger.getLogger(Singleton1Connexion_Singleton.class.getSimpleName());
    private Connection connection;
    private String url = "jdbc:postgresql://localhost:5432/dbtest";
    private String user = "test";
    private String password = "test";

            private Singleton1Connexion_Singleton(){
                try {
                        Class.forName("org.postgresql.Driver");
                        this.connection= DriverManager.getConnection(url, user, password);
                    } catch (Exception e) {
                        log.error(Singleton1Connexion_Singleton.class.getSimpleName() + ":" + "Database Connection Creation Failed : " + e.getMessage());
                    }
            }
            public Connection getCon(){
                return connection;
            }
            public static Singleton1Connexion_Singleton getInstance(){
                if(singletonInstance == null) {
                        singletonInstance = new Singleton1Connexion_Singleton();
                    } else
                        try {
                           if(singletonInstance.getCon().isClosed()) {
                                    singletonInstance = new Singleton1Connexion_Singleton();
                                }
                        } catch (SQLException e) {
                            log.error(Singleton1Connexion_Singleton.class.getSimpleName() + ":" + "Database Connection Creation Failed : " + e.getMessage());
                        }
                return singletonInstance;
            }
}

